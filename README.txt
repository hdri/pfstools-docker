The Dockerfile file contains instructions for automatically building a Docker container with pfstools toolset for image manipulation.
To build the container, run the following command in current folder:

	docker build . -t pfstools_tmp

this will create a new container with tag/name "pfstools_tmp", ready to beused. 
