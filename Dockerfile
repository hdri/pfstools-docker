# $ docker build . -t pfstools_tmp

FROM debian:9

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -yq && apt-get install -yq wget cmake libmagick++-6.q16hdri-7 g++ zlib1g-dev libopenexr-dev libmagick++-dev patch imagemagick

COPY pfstools_pfsouthdrhtml_do_not_round_exposure.patch /tmp/

RUN wget https://downloads.sourceforge.net/project/pfstools/pfstools/2.1.0/pfstools-2.1.0.tgz
RUN tar xf pfstools-2.1.0.tgz &&\
rm pfstools-2.1.0.tgz &&\
cd pfstools-2.1.0 &&\
patch -p1 < /tmp/pfstools_pfsouthdrhtml_do_not_round_exposure.patch &&\
mkdir build &&\
cd build &&\
cmake .. &&\
make &&\
make install
# mount a directory to work in when running the container:
# $ docker run -v /host/directory:/workdir <image_name> <command_to_run>
WORKDIR /workdir

